
((factory) => {
	if (typeof define === 'function' && define.amd) {
		define([
			'@hxss/recursive-proxy',
			'selector-getter'
		], factory);
	} else if (typeof exports !== 'undefined') {
		module.exports = factory(
			require('@hxss/recursive-proxy'),
			require('selector-getter')
		);
	} else {
		factory(RecursiveProxy, SelectorGetter);
	}
})((RecursiveProxy, SelectorGetter) => {

	class hPlug {

		defs() {
			this.node = null;
			this.settings = {};
		}

		constructor(node, settings) {
			if (node[this.lowerName])
				return node[this.lowerName];

			hPlug.prototype.defs();
			this.defs();

			this.node = jqueryAvailable()
				? $(node)
				: [node];
			this.node[0][this.lowerName] = this;

			this.initSettings(settings);
			this.initHandlers();

			return this.init();
		}

		timer(func, timout = 250) {
			var timer = 0;

			return () => {
				clearTimeout(timer);
				timer = setTimeout(
					this.h[func],
					timout
				);
			};
		}

		initSettings(settings) {
			this.settings = Object.assign(this.settings || {}, settings);
			this.s = new RecursiveProxy(this.settings, {
				get: function(target, prop) {
					return typeof target[prop] == 'string'
						? new SelectorGetter(target[prop])
						: target[prop];
				}
			});
		}

		initHandlers() {
			this.handler = {};
			this.h = new Proxy(this, {
				get: function(obj, func) {
					if (!obj.handler.hasOwnProperty(func)
						&& typeof obj[func] === 'function'
						&& func.charAt(0) !== '_'
					) {
						obj.handler[func] = function() {
							var args = [this].concat(Array.from(arguments));
							return obj[func].apply(obj, args);
						};;
					}

					return obj.handler[func];
				}
			});
		}

		addEventListener(type, handler) {
			this.node[0].addEventListener(type, handler);
		}

		removeEventListener(type, handler) {
			this.node[0].removeEventListener(type, handler);
		}

		destruct() {
			delete this.node[0][this.lowerName];

			return {};
		}

		get lowerName() {
			if (!this._lowerName) {
				this._lowerName = this.constructor.lowerName();
			}

			return this._lowerName;
		}

		static lowerName() {
			return lcfirst(this.name);
		}

		static register() {
			this.registerEvents();
			this.registerConstructs();
		}

		static registerEvents() {
			var descriptors = Object.getOwnPropertyDescriptors(
				this.prototype
			);
			Object.keys(descriptors)
				.forEach(function(key) {
					if (['constructor', 'defs'].indexOf(key) === -1
						&& key.charAt(0) !== '_'
						&& descriptors[key].get === undefined
						&& descriptors[key].set === undefined
					) {
						this.prototype[key] = this.eventHandler(
							key, this.prototype[key]
						);
					}
				}, this);
		}

		static eventHandler(key, func) {
			return function() {
				key = ucfirst(key);
				var node = this.node instanceof Element
					? this.node
					: this.node[0];
				var settings = {
					bubbles: false,
					detail: {
						[this.lowerName]: this,
						args: arguments,
					},
				};

				var before = new CustomEvent('before' + key, settings);
				node.dispatchEvent(before);

				settings.detail.result = func.apply(this, arguments);

				var after  = new CustomEvent('after'  + key, settings);
				node.dispatchEvent(after);

				return settings.detail.result;
			}
		}

		static registerConstructs() {
			var theClass = this;
			var name = this.name;

			Element.prototype[name] = function(settings) {
				return new theClass(this, settings);
			};
			if (jqueryAvailable()) {
				$.fn[this.lowerName()] = function(settings) {
					return $(this).map(function() {
						return this[name](settings);
					});
				};
			}
		}
	}

	function jqueryAvailable() {
		return typeof $ !== 'undefined'
			&& $.prototype.hasOwnProperty('jquery');
	}

	function ucfirst(str) {
		return str[0].toUpperCase() + str.slice(1);
	}
	function lcfirst(str) {
		return str[0].toLowerCase() + str.slice(1);
	}

	if (typeof window !== 'undefined')
		window.hPlug = hPlug;

	return hPlug;

});
