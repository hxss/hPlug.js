# hPlug.js

Simple js plugin template.

## Futures

### Events
* automatic register events around methods(`beforeInit`, `afterInit`, ...);

hPlug uses js [CustomEvent](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent) that dispatches to main `Element`(the element on that constructor was called). Event bubbling disabled. On `Event` details puts instance and arguments for current method:
```js
{
	bubbles: false,
	detail: {
		[this.lowerName]: this,
		args: arguments,
	},
}
```
(where `lowerName` - class name with first letter in lower case)

For `after` event in `detail.result` also puts value returned by method.

The class implement `addEventListener` and `removeEventListener` methods for chain calls support.

### Constructors
* register js `Element` and `$.fn` constructors for your class(`Element.prototype[ClassName]`, `$.fn[this.lowerName]`);

After class definition you need to call static method `register` that register constructors and methods events.

### Methods handlers
* `this.h.methodName` - handler `Proxy` that return method with binded `this` value that you can pass in any function/event and can be sure, that `this` will refer to class object without loss of arguments. Method runned by handler will receive default `this` object as first argument and `Event` object as second. Example:

### Timer
* If you want to subscribe to event that occurs too much(resize, scroll) - you can use `timer(methodName[, timeout = 250])` that will call your method not more often than once at `timeout`ms.

### Settings proxy
* `this.s.settingName` - settings [RecursiveProxy](https://gitlab.com/hxss/RecursiveProxy) that return [SelectorGetter](gitlab.com/hxss/SelectorGetter.js) for every string value. see [Life cycle](#lifecycle).


## <a name="lifecycle"></a>Lifecycle/Extending

In most cases you no need to use `constructor` in your class. What you need - is implement 2 methods: `defs` and `init`.

```js
Class MyPlugin extends hPlug {
	// this method exist because of
	// ES6 dont allow to define class members in class body
	defs() {
		this.settings = {
			// default setting that will be extended by args
			sname1: 'sval1',
			sname2: 'sval2',
		};

		this.myField = 'defaultValue';
	}

	init() {
		// init method has a role of constructor here
		// and runs at the end of hPlug constructor
		return this;
	}
}
```

### hPlug constructor and settings

hPlug constructor receive node(Element onthat constructor called) and settings objects.

1. At the first stage hPlug check if the node already has assigned class object and if it exist - return this object. Otherwise the constructing will continue.

2. `defs` method will be called.
3. the node will saved into `this.node` field:
```js
this.node = jqueryAvailable()
	? $(node)
	: [node];
```
4. `this` will be assigned to node:
```js
this.node[0][this.lowerName] = this;
```
5. Settings arg will be assigned to default values that you define in `defs` method. Also will be initialized `this.s` - [RecursiveProxy](https://gitlab.com/hxss/RecursiveProxy) that reflect `this.settings` object with one difference - every `String` values will be returned as [SelectorGetter](gitlab.com/hxss/SelectorGetter.js). So if you need to use pure string value of setting, you can use `this.settings.sname1` or `this.s.sname1.s`.

6. `destruct` method will unsign this object from node and return empty object.

## Example

```js

class ContentPlugin extends hPlug {

	defs() {
		this.node = null;
		this.settings = {
			sname1: 'sval1',
		};
	}

	init() {
		this.node.click(this.h.func);

		return this.node[0];
		return this;
		return {};
	}

	func(arg) {
		console.log(this.s.sname2.s); // 'sval2'
	}
}
ContentPlugin.register();

content.on('beforeInit', function(e) {
	console.log('beforeInit', arguments);
});
content.on('afterInit', function(e) {
	console.log('afterInit', arguments);
});

// jQuery constructor:
var content = $('.content');
var obj = content.contentPlugin({
	sname2: 'sval2',
});
console.log(obj[0].contentPlugin); // ContentPlugin {}

// js Element constructor:
var content = document.getElementsByClassName('content');
var obj = content[0].ContentPlugin({
	sname2: 'sval2',
});
console.log(obj.contentPlugin); // ContentPlugin {}
```
